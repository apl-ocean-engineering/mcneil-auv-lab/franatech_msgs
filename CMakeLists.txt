cmake_minimum_required(VERSION 3.0.2)
project(franatech_msgs)

find_package(catkin REQUIRED COMPONENTS
  genmypy
  message_generation
  std_msgs
)

add_message_files(
  FILES
  Mets.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
 CATKIN_DEPENDS genmypy message_runtime std_msgs
)
